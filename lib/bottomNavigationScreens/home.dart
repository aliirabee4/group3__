// import 'package:flutter/material.dart';

// class HomePage extends StatefulWidget {
//   @override
//   _HomePageState createState() => _HomePageState();
// }

// class _HomePageState extends State<HomePage> {
//   Widget _icon({IconData iconData, Function onTap}) {
//     return IconButton(
//         icon: Icon(
//           iconData,
//           color: Colors.black,
//         ),
//         onPressed: onTap);
//   }

//   Widget _catItem({String catName}) {
//     return Container(
//       width: 100,
//       height: 30,
//       margin: EdgeInsets.symmetric(horizontal: 5, vertical: 15),
//       decoration: BoxDecoration(
//           border: Border.all(width: 1, color: Colors.grey),
//           borderRadius: BorderRadius.circular(4),
//           color: Colors.white),
//       child: Center(
//         child: Text(
//           catName,
//           style: TextStyle(color: Colors.grey),
//         ),
//       ),
//     );
//   }

//   Widget _productCard() {
//     return Container(
//       decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(8),
//           color: Colors.greenAccent.withOpacity(0.3)),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.end,
//         children: [
//           Container(
//             width: 45,
//             height: 45,
//             decoration: BoxDecoration(
//               color: Colors.white.withOpacity(0.3),
//               borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20)),
//             ),
//             child: Center(
//               child: Icon(
//                 Icons.favorite_border,
//                 color: Colors.grey,
//               ),
//             ),
//           ),
//           Center(
//             child: Container(
//               width: 100,
//               height: 100,
//               decoration: BoxDecoration(
//                   image: DecorationImage(
//                       image: NetworkImage(
//                           'https://img.freepik.com/free-vector/red-apple-with-happy-face_1308-9211.jpg?size=626&ext=jpg&ga=GA1.2.205326237.1597520898'),
//                       fit: BoxFit.cover),
//                   color: Colors.white,
//                   borderRadius: BorderRadius.circular(50)),
//             ),
//           ),
//           Expanded(child: SizedBox()),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Column(
//                 children: [
//                   Text(
//                     'Apple',
//                     style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//                   ),
//                   Text('120 LE/kg')
//                 ],
//               ),
//               Container(
//                 width: 45,
//                 height: 45,
//                 decoration: BoxDecoration(
//                   color: Colors.white.withOpacity(0.3),
//                   borderRadius:
//                       BorderRadius.only(bottomLeft: Radius.circular(20)),
//                 ),
//                 child: Center(
//                   child: Icon(
//                     Icons.add,
//                     color: Colors.grey,
//                   ),
//                 ),
//               ),
//             ],
//           )
//         ],
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         elevation: 0,
//         leading: _icon(iconData: Icons.arrow_back, onTap: () {}),
//         actions: [
//           _icon(iconData: Icons.search, onTap: () {}),
//           _icon(iconData: Icons.shopping_cart, onTap: () {})
//         ],
//       ),
//       body: Column(
//         children: [
//           SizedBox(
//             height: 70,
//             child: ListView.builder(
//                 scrollDirection: Axis.horizontal,
//                 itemCount: 5,
//                 itemBuilder: (ctx, index) {
//                   return _catItem(catName: 'Cat${index + 1}');
//                 }),
//           ),
//           Expanded(
//             child: Padding(
//               padding: const EdgeInsets.all(10),
//               child: GridView.builder(
//                   itemCount: 9,
//                   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                       crossAxisCount: 2,
//                       childAspectRatio: 0.8,
//                       crossAxisSpacing: 10,
//                       mainAxisSpacing: 10),
//                   itemBuilder: (ctx, index) {
//                     return _productCard();
//                   }),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
