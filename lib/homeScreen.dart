// import 'package:flutter/material.dart';
// import 'package:group_3/bottomNavigationScreens/home.dart';
// import 'package:group_3/bottomNavigationScreens/msg.dart';
// import 'package:group_3/bottomNavigationScreens/notification.dart';
// import 'package:group_3/bottomNavigationScreens/search.dart';

// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   int _currentIndex = 0;
//   List _pages = [HomePage(), SearchPage(), NotificationPage(), MsgPage()];

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: _pages[_currentIndex],
//       bottomNavigationBar: BottomNavigationBar(
//         type: BottomNavigationBarType.fixed,
//         selectedItemColor: Colors.red,
//         onTap: (index) {
//           setState(() {
//             _currentIndex = index;
//           });
//         },
//         currentIndex: _currentIndex,
//         items: [
//           BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.home,
//                 size: 30,
//               ),
//               title: Text(
//                 '',
//                 style: TextStyle(fontSize: 0),
//               )),
//           BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.search,
//                 size: 30,
//               ),
//               title: Text("", style: TextStyle(fontSize: 0))),
//           BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.notifications,
//                 size: 30,
//               ),
//               title: Text("", style: TextStyle(fontSize: 0))),
//           BottomNavigationBarItem(
//               icon: Icon(
//                 Icons.mail_outline,
//                 size: 30,
//               ),
//               title: Text("", style: TextStyle(fontSize: 0)))
//         ],
//       ),
//     );
//   }
// }
