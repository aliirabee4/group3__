import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class FlareHome extends StatefulWidget {
  @override
  _FlareHomeState createState() => _FlareHomeState();
}

class _FlareHomeState extends State<FlareHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FlareActor('assets/smiley_switch.flr'),
    );
  }
}
