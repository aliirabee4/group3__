//import 'package:flutter/material.dart';
//import 'package:group_3/product_details.dart';
//
//class HomeScreen extends StatefulWidget {
//  @override
//  _HomeScreenState createState() => _HomeScreenState();
//}
//
//class _HomeScreenState extends State<HomeScreen> {
//  List<Map<String, dynamic>> _data = [
//    {
//      'title': 'Breakfast',
//      'isSelected': true,
//      'image':
//          'https://image.freepik.com/free-photo/pile-fresh-fruits_144627-17253.jpg'
//    },
//    {
//      'title': 'Lunch',
//      'isSelected': false,
//      'image':
//          'https://image.freepik.com/free-photo/pile-fresh-fruits_144627-17253.jpg'
//    },
//    {
//      'title': 'Dinner',
//      'isSelected': false,
//      'image':
//          'https://image.freepik.com/free-photo/pile-fresh-fruits_144627-17253.jpg'
//    }
//  ];
//  int _selectedCard = 0;
//
//  Widget _catCard({String title, String imageUrl, int index, bool selected}) {
//    return InkWell(
//      onTap: () {
//        setState(() {
//          _data[_selectedCard]['isSelected'] = false;
//          _selectedCard = index;
//          _data[_selectedCard]['isSelected'] = true;
//        });
//      },
//      child: Container(
//        width: 90,
//        height: 200,
//        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
//        decoration: BoxDecoration(
//            borderRadius: BorderRadius.circular(50),
//            color: selected ? Colors.red : Colors.white),
//        child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceAround,
//          children: [
//            Container(
//              width: 60,
//              height: 60,
//              decoration: BoxDecoration(
//                  borderRadius: BorderRadius.circular(50),
//                  color: Colors.white,
//                  image: DecorationImage(image: NetworkImage(imageUrl))),
//            ),
//            Text(
//              title,
//              style: TextStyle(color: Colors.white),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  Widget _productCard() {
//    return InkWell(
//      onTap: () {
//        Navigator.push(context,
//            MaterialPageRoute(builder: (context) => ProductDetailsScreen()));
//      },
//      child: Container(
//        width: MediaQuery.of(context).size.width / 3,
//        height: 200,
//        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
//        decoration: BoxDecoration(
//            borderRadius: BorderRadius.circular(5), color: Colors.amberAccent),
//      ),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      backgroundColor: Colors.white,
//      appBar: AppBar(
//        backgroundColor: Colors.white,
//        elevation: 0,
//        leading: Container(
//          width: 20,
//          height: 20,
//          margin: EdgeInsets.all(8),
//          decoration: BoxDecoration(
//              borderRadius: BorderRadius.circular(7), color: Colors.red),
//          child: Center(
//            child: Icon(
//              Icons.menu,
//              color: Colors.white,
//            ),
//          ),
//        ),
//        actions: [
//          IconButton(
//              icon: Icon(
//                Icons.search,
//                color: Colors.black,
//              ),
//              onPressed: null),
//          IconButton(
//              icon: Icon(
//                Icons.cloud_queue,
//                color: Colors.black,
//              ),
//              onPressed: null),
//        ],
//      ),
//      body: Column(
//        children: [
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 25),
//            child: Row(
//              children: [
//                Text(
//                  'Food',
//                  style: TextStyle(
//                      color: Colors.black,
//                      fontSize: 25,
//                      fontWeight: FontWeight.bold),
//                )
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
//            child: Row(
//              children: [
//                Text(
//                  'Calories',
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontSize: 25,
//                  ),
//                )
//              ],
//            ),
//          ),
//          SizedBox(
//            height: 200,
//            child: ListView.builder(
//              scrollDirection: Axis.horizontal,
//              itemCount: 3,
//              itemBuilder: (ctx, index) {
//                return _catCard(
//                    title: _data[index]['title'],
//                    selected: _data[index]['isSelected'],
//                    index: index,
//                    imageUrl: _data[index]['image']);
//              },
//            ),
//          ),
//          SizedBox(
//            height: 200,
//            child: ListView.builder(
//                itemCount: 10,
//                scrollDirection: Axis.horizontal,
//                itemBuilder: (ctx, index) {
//                  return _productCard();
//                }),
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 20),
//            child: Row(
//              children: [
//                Text(
//                  'Food',
//                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
//                ),
//                Text(
//                  'Benefits',
//                  style: TextStyle(fontSize: 18),
//                ),
//              ],
//            ),
//          )
//        ],
//      ),
//    );
//  }
//}
