import 'package:flutter/material.dart';
import 'package:group_3/passing_data/details.dart';
import 'package:group_3/passing_data/sliver.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SliverScreen()));
              },
              child: Text('Go To Next Screen'))),
    );
  }
}
