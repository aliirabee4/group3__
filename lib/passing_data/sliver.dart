import 'package:flutter/material.dart';

class SliverScreen extends StatefulWidget {
  @override
  _SliverScreenState createState() => _SliverScreenState();
}

class _SliverScreenState extends State<SliverScreen> {
  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
        body: Container(
          color: Colors.white,
        ),
        headerSliverBuilder: (ctx, value) {
          return [
            SliverAppBar(
              backgroundColor: Colors.red,
              flexibleSpace: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.teal,
                        image: DecorationImage(
                            image: NetworkImage(
                                'https://image.freepik.com/free-photo/empty-living-room-with-blue-sofa-plants-table-empty-white-wall-background-3d-rendering_41470-1778.jpg'),
                            fit: BoxFit.cover)),
                  ),
                  Container(
                    color: Colors.black.withOpacity(0.4),
                  )
                ],
              ),
              title: Text('Sliver'),
              //  actions: [Icon(Icons.home)],
              pinned: true,
              floating: true,
              leading: Icon(Icons.arrow_back),
              expandedHeight: 200,
            )
          ];
        });
  }
}
