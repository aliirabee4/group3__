// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:geolocator/geolocator.dart';

// class MapScreen extends StatefulWidget {
//   @override
//   _MapScreenState createState() => _MapScreenState();
// }

// class _MapScreenState extends State<MapScreen> {
//   Completer<GoogleMapController> _controller = Completer();

//   double _currentLat;
//   double _currentLong;

//   @override
//   void initState() {
//     _getCurrentLocation();
//     super.initState();
//   }

//   _getCurrentLocation() async {
//     Position position = await Geolocator()
//         .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
//     setState(() {
//       _currentLat = position.latitude;
//       _currentLong = position.longitude;
//     });
//   }

//   _getAddressFromLatLong({double lat, double long}) async {
//     List<Placemark> placemark =
//         await Geolocator().placemarkFromCoordinates(lat, long);
//     print(placemark[0].country);
//     print(placemark[0].administrativeArea);
//     print(placemark[0].subAdministrativeArea);
//     print(placemark[0].name);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: _currentLat == null
//           ? Center(child: CircularProgressIndicator())
//           : GoogleMap(
//               mapType: MapType.normal,
//               initialCameraPosition: CameraPosition(
//                   target: LatLng(_currentLat, _currentLong), zoom: 15),
//               onMapCreated: (controller) {
//                 _controller.complete(controller);
//               },
//               onTap: (latLong) {
//                 _getAddressFromLatLong(
//                     lat: latLong.latitude, long: latLong.longitude);
//               },
//             ),
//     );
//   }
// }
