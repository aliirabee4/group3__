// import 'package:flutter/material.dart';
// import 'package:group_3/home_app_bar.dart';
// import 'package:group_3/rest_list.dart';
// import 'package:group_3/textField.dart';

// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Theme.of(context).backgroundColor,
//       appBar: homeAppBar(context: context),
//       body: Column(
//         children: [customTextField(context), restList(context: context)],
//       ),
//     );
//   }
// }
