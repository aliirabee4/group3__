import 'package:flutter/material.dart';
import 'package:group_3/session11/add_button.dart';
import 'package:group_3/session11/person_card.dart';

class SendAgain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 110,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Send Again',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
          SizedBox(
            height: 60,
            child: Row(
              children: [
                AddButton(),
                Expanded(
                  child: ListView.builder(
                      itemCount: 10,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return PersonCard();
                      }),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
