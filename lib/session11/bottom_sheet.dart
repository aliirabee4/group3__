import 'package:flutter/material.dart';

int _counter = 1;
bottomSheet({BuildContext context}) {
  showModalBottomSheet(
      context: context,
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      builder: (context) {
        return StatefulBuilder(builder: (context, set) {
          return Container(
            height: MediaQuery.of(context).size.height / 2,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(50),
                    topLeft: Radius.circular(50))),
            child: Row(
              children: [
                IconButton(
                    icon: Icon(Icons.add),
                    onPressed: () {
                      set(() {
                        _counter++;
                      });
                    }),
                Text(_counter.toString())
              ],
            ),
          );
        });
      });
}
