import 'package:flutter/material.dart';

import 'bottom_sheet.dart';

class PersonCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        bottomSheet(context: context);
      },
      child: Container(
        width: 50,
        height: 50,
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Colors.grey,
            image: DecorationImage(
                image: NetworkImage(
                    'https://t3.ftcdn.net/jpg/01/83/48/62/240_F_183486295_fT7OZbA5o1balBSyqvbwUsdADeFOxjsS.jpg'))),
      ),
    );
  }
}
