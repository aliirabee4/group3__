import 'package:flutter/material.dart';

class AddButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.teal,
       ),
      child: Center(child: Icon(Icons.add, color: Colors.white,size: 30,),),
    );
  }
}
