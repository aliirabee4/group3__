// import 'dart:io';
// import 'package:flutter/material.dart';
// import 'package:image_picker/image_picker.dart';

// class CameraScreen extends StatefulWidget {
//   @override
//   _CameraScreenState createState() => _CameraScreenState();
// }

// class _CameraScreenState extends State<CameraScreen> {
//   File _image;
//   final picker = ImagePicker();

//   Future getImage(ImageSource source) async {
//     final pickedFile = await picker.getImage(source: source);
//     setState(() {
//       _image = File(pickedFile.path);
//       print(_image);
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: Column(
//       children: [
//         Center(
//           child: InkWell(
//             onTap: () {
//               getImage(ImageSource.gallery);
//             },
//             child: Container(
//               width: 120,
//               height: 50,
//               decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5), color: Colors.teal),
//               child: Center(
//                 child: Text(
//                   'Gallery',
//                   style: TextStyle(color: Colors.white, fontSize: 18),
//                 ),
//               ),
//             ),
//           ),
//         ),
//         Center(
//           child: InkWell(
//             onTap: () {
//               getImage(ImageSource.camera);
//             },
//             child: Container(
//               width: 120,
//               height: 50,
//               decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5), color: Colors.teal),
//               child: Center(
//                 child: Text(
//                   'Camera',
//                   style: TextStyle(color: Colors.white, fontSize: 18),
//                 ),
//               ),
//             ),
//           ),
//         ),
//         SizedBox(
//           height: 100,
//         ),
//         _image == null
//             ? Container()
//             : Container(
//                 width: MediaQuery.of(context).size.width,
//                 height: 200,
//                 decoration: BoxDecoration(
//                     image: DecorationImage(
//                         image: FileImage(_image), fit: BoxFit.cover)),
//               )
//       ],
//     ));
//   }
// }
