// import 'package:flutter/material.dart';
// import 'package:group_3/tabpar/calls.dart';
// import 'package:group_3/tabpar/camera.dart';
// import 'package:group_3/tabpar/chats.dart';
// import 'package:group_3/tabpar/status.dart';

// class HomEScreen extends StatefulWidget {
//   @override
//   _HomEScreenState createState() => _HomEScreenState();
// }

// class _HomEScreenState extends State<HomEScreen>
//     with SingleTickerProviderStateMixin {
//   TabController _tabController;
//   @override
//   void initState() {
//     _tabController = TabController(length: 4, vsync: this);
//     super.initState();
//   }

//   int _currentIndex = 0;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           'WhatsApp',
//           style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
//         ),
//         backgroundColor: Colors.teal,
//         centerTitle: false,
//         bottom: TabBar(
//             isScrollable: true,
//             onTap: (index) {
//               setState(() {
//                 _currentIndex = index;
//               });
//             },
//             controller: _tabController,
//             unselectedLabelColor: Colors.black,
//             labelStyle: TextStyle(fontSize: 16),
//             indicatorColor: (() {
//               if (_currentIndex == 0) {
//                 return Colors.transparent;
//               } else {
//                 return Colors.white;
//               }
//             }()),
//             tabs: [
//               Tab(
//                 icon: Icon(
//                   Icons.camera_alt,
//                   size: 20,
//                 ),
//               ),
//               Container(
//                 width: MediaQuery.of(context).size.width / 4 - 15,
//                 child: Tab(
//                   text: 'Chats',
//                 ),
//               ),
//               Container(
//                 width: MediaQuery.of(context).size.width / 4 - 15,
//                 child: Tab(
//                   text: 'Status',
//                 ),
//               ),
//               Container(
//                 width: MediaQuery.of(context).size.width / 4 - 15,
//                 child: Tab(
//                   text: 'Calls',
//                 ),
//               )
//             ]),
//       ),
//       body: TabBarView(controller: _tabController, children: [
//         CameraScreen(),
//         ChatsScreen(),
//         StatusScreen(),
//         CallsScreen()
//       ]),
//     );
//   }
// }
