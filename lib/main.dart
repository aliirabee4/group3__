import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:group_3/session15/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // int _currentStep = 0;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    _firebaseMessaging.getToken().then((token) => print(token));
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {},
      onLaunch: (Map<String, dynamic> message) async {},
      onResume: (Map<String, dynamic> message) async {},
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            fontFamily: 'Tajawal',
            accentColor: Color.fromRGBO(224, 131, 180, 1),
            primaryColor: Color.fromRGBO(67, 85, 160, 1),
            backgroundColor: Color.fromRGBO(247, 247, 247, 1)),
        home: HomePage()
        //  Scaffold(
        //   body: ListView(
        //     children: [
        //       Stepper(
        //           currentStep: _currentStep,
        //           onStepTapped: (index) {
        //             setState(() {
        //               _currentStep = index;
        //             });
        //           },
        //           onStepCancel: () {
        //             print('Cancel');
        //           },
        //           onStepContinue: () {
        //             setState(() {
        //               _currentStep++;
        //             });
        //           },
        //           steps: [
        //             Step(
        //                 isActive: true,
        //                 title: Text('First Step'),
        //                 content: Icon(
        //                   Icons.home,
        //                   color: Colors.red,
        //                 )),
        //             Step(title: Text('Secend Step'), content: Icon(Icons.home)),
        //           ])
        //     ],
        //   ),
        // ),
        );
  }
}
