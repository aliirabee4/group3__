import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController _controller;
  @override
  void initState() {
    _controller = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: Icon(Icons.add),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                ),
                title: Text('.')),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.notifications,
                ),
                title: Text('.')),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.notifications,
                ),
                title: Text('.')),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.favorite,
                ),
                title: Text('.'))
          ],
        ),
        appBar: AppBar(
          centerTitle: true,
          title: Text('Rating App'),
        ),
        body: Column(
          children: [
            TabBar(
                indicatorColor: Colors.transparent,
                isScrollable: false,
                controller: _controller,
                unselectedLabelStyle:
                    TextStyle(color: Colors.grey, fontSize: 14),
                tabs: [
                  Text(
                    'data',
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ),
                  Text(
                    'data',
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ),
                  Text(
                    'data',
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ),
                ]),
            Expanded(
              child: TabBarView(controller: _controller, children: [
                Container(
                  color: Colors.red,
                ),
                Container(
                  color: Colors.teal,
                ),
                Container(
                  color: Colors.yellow,
                ),
              ]),
            )
          ],
        ));
  }
}
